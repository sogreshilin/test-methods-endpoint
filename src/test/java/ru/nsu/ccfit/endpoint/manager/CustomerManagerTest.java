package ru.nsu.ccfit.endpoint.manager;

import java.util.UUID;

import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import ru.nsu.ccfit.endpoint.AbstractTest;
import ru.nsu.ccfit.endpoint.database.IDBService;
import ru.nsu.ccfit.endpoint.database.data.CustomerPojo;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

class CustomerManagerTest extends AbstractTest {
    private IDBService dbService;
    private CustomerManager customerManager;

    private CustomerPojo createCustomerInput;

    @BeforeEach
    void init() {
        // create stubs for the test's class
        dbService = mock(IDBService.class);
        Logger logger = mock(Logger.class);

        // create the test's class
        customerManager = new CustomerManager(dbService, logger);
    }

    @Test
    void createCustomer(SoftAssertions softly) {
        // настраиваем mock.
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "Baba_Jaga";
        createCustomerInput.balance = 0;

        CustomerPojo createCustomerOutput = new CustomerPojo();
        createCustomerOutput.id = UUID.randomUUID();
        createCustomerOutput.firstName = "John";
        createCustomerOutput.lastName = "Wick";
        createCustomerOutput.login = "john_wick@gmail.com";
        createCustomerOutput.pass = "Baba_Jaga";
        createCustomerOutput.balance = 0;

        when(dbService.createCustomer(createCustomerInput)).thenReturn(createCustomerOutput);

        // Вызываем метод, который хотим протестировать
        CustomerPojo customer = customerManager.createCustomer(createCustomerInput);

        // Проверяем результат выполенния метода
        softly.assertThat(customer.id).isEqualTo(createCustomerOutput.id);

        // Проверяем, что метод по созданию Customer был вызван ровно 1 раз с определенными аргументами
        verify(dbService, times(1)).createCustomer(createCustomerInput);

        // Проверяем, что другие методы не вызывались...
        verify(dbService, times(0)).getCustomers();
    }

    @Test
    void createCustomerWithNullArgument_Right(SoftAssertions softly) {
        softly.assertThatThrownBy(() -> customerManager.createCustomer(null))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessageContaining("Argument 'customerData' is null.");
    }

    @Test
    void createCustomerWithWhitespaceInFirstName(SoftAssertions softly) {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "Joh n";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "123qwee";
        createCustomerInput.balance = 0;

        softly.assertThatThrownBy(() -> customerManager.createCustomer(createCustomerInput))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessageContaining("firstName should not contains whitespaces");
    }

    @Test
    void createCustomerWithShortFirstName(SoftAssertions softly) {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "J";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "123qwee";
        createCustomerInput.balance = 0;

        softly.assertThatThrownBy(() -> customerManager.createCustomer(createCustomerInput))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessageContaining("firstName's length should be more or equal 6 symbols and less or equal 12 symbols.");
    }

    @Test
    void createCustomerWithIllegalFirstName_capitalLetter(SoftAssertions softly) {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "john";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "123qwee";
        createCustomerInput.balance = 0;

        softly.assertThatThrownBy(() -> customerManager.createCustomer(createCustomerInput))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessageContaining("firstName's should begin with a capital letter, the rest of the characters are lowercase.");
    }

    @Test
    void createCustomerWithIllegalFirstName_illegalSymbols(SoftAssertions softly) {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John-Fg31";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "123qwee";
        createCustomerInput.balance = 0;

        softly.assertThatThrownBy(() -> customerManager.createCustomer(createCustomerInput))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessageContaining("firstName's should begin with a capital letter, the rest of the characters are lowercase.");
    }

    @Test
    void createCustomerWithWhitespaceInLastName(SoftAssertions softly) {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wic k";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "123qwee";
        createCustomerInput.balance = 0;

        softly.assertThatThrownBy(() -> customerManager.createCustomer(createCustomerInput))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessageContaining("lastName should not contains whitespaces");
    }

    @Test
    void createCustomerWithShortLastName(SoftAssertions softly) {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "W";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "123qwee";
        createCustomerInput.balance = 0;

        softly.assertThatThrownBy(() -> customerManager.createCustomer(createCustomerInput))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessageContaining("lastName's length should be more or equal 6 symbols and less or equal 12 symbols.");
    }

    @Test
    void createCustomerWithIllegalLastName_capitalLetter(SoftAssertions softly) {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "123qwee";
        createCustomerInput.balance = 0;

        softly.assertThatThrownBy(() -> customerManager.createCustomer(createCustomerInput))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessageContaining("lastName's should begin with a capital letter, the rest of the characters are lowercase.");
    }

    @Test
    void createCustomerWithIllegalLastName_illegalSymbols(SoftAssertions softly) {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick-Wdsf";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "123qwee";
        createCustomerInput.balance = 0;

        softly.assertThatThrownBy(() -> customerManager.createCustomer(createCustomerInput))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessageContaining("lastName's should begin with a capital letter, the rest of the characters are lowercase.");
    }

    @Test
    void createCustomerWithInvalidLogin(SoftAssertions softly) {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wickgmail.com";
        createCustomerInput.pass = "123qwee";
        createCustomerInput.balance = 0;

        softly.assertThatThrownBy(() -> customerManager.createCustomer(createCustomerInput))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessageContaining("login should be valid email");
    }

    @Test
    void createCustomerWithExistingLogin(SoftAssertions softly) {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "123qwee";
        createCustomerInput.balance = 0;

        when(dbService.getCustomerIdByLogin("john_wick@gmail.com")).thenReturn(UUID.randomUUID());

        softly.assertThatThrownBy(() -> customerManager.createCustomer(createCustomerInput))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessageContaining("Customer with login john_wick@gmail.com already exists");

        verify(dbService, times(1)).getCustomerIdByLogin("john_wick@gmail.com");
        verifyNoMoreInteractions(dbService);
    }

    @Test
    void createCustomerWithShortPassword(SoftAssertions softly) {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "123qw";
        createCustomerInput.balance = 0;

        softly.assertThatThrownBy(() -> customerManager.createCustomer(createCustomerInput))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessageContaining("Password's length should be more or equal 6 symbols and less or equal 12 symbols.");
    }

    @Test
    void createCustomerWithEasyPassword(SoftAssertions softly) {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "123qwe";
        createCustomerInput.balance = 0;

        softly.assertThatThrownBy(() -> customerManager.createCustomer(createCustomerInput))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessageContaining("Password is easy.");
    }

    @Test
    void createCustomerWithPositiveBalance(SoftAssertions softly) {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "123qwee";
        createCustomerInput.balance = 1;

        softly.assertThatThrownBy(() -> customerManager.createCustomer(createCustomerInput))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessageContaining("balance should be zero");
    }

    @Test
    void createCustomerWithNegativeBalance(SoftAssertions softly) {
        createCustomerInput = new CustomerPojo();
        createCustomerInput.firstName = "John";
        createCustomerInput.lastName = "Wick";
        createCustomerInput.login = "john_wick@gmail.com";
        createCustomerInput.pass = "123qwee";
        createCustomerInput.balance = 1;

        softly.assertThatThrownBy(() -> customerManager.createCustomer(createCustomerInput))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessageContaining("balance should be zero");
    }
}
