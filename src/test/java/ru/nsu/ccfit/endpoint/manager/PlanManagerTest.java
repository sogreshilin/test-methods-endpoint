package ru.nsu.ccfit.endpoint.manager;

import java.util.UUID;

import org.apache.commons.lang.RandomStringUtils;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import ru.nsu.ccfit.endpoint.AbstractTest;
import ru.nsu.ccfit.endpoint.database.IDBService;
import ru.nsu.ccfit.endpoint.database.data.PlanPojo;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PlanManagerTest extends AbstractTest {
    private IDBService dbService;
    private PlanManager planManager;
    private PlanPojo plan = new PlanPojo();

    @BeforeEach
    void init() {
        dbService = mock(IDBService.class);
        Logger logger = mock(Logger.class);
        planManager = new PlanManager(dbService, logger);
    }

    @Test
    void testCreatePlan_Success(SoftAssertions softly) {
        fillDefaultPlan(plan);

        PlanPojo expectedCreatedPlan = new PlanPojo();
        fillDefaultPlan(expectedCreatedPlan);
        expectedCreatedPlan.id = UUID.randomUUID();

        when(dbService.createPlan(plan)).thenReturn(expectedCreatedPlan);

        PlanPojo actualCreatedPlan = planManager.createPlan(this.plan);

        softly.assertThat(actualCreatedPlan.id).isEqualTo(expectedCreatedPlan.id);

        verify(dbService, times(1)).getPlanIdByName(plan.name);
        verify(dbService, times(1)).createPlan(plan);
    }

    @Test
    void testCreatePlan_ValidationError_InvalidName(SoftAssertions softly) {
        fillDefaultPlan(plan);
        plan.name = RandomStringUtils.random(129);
        softly.assertThatThrownBy(() -> planManager.createPlan(plan))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage("Argument 'plan.name' must be between 2 and 128 symbols");
        verify(dbService, times(0)).getPlanIdByName(plan.name);
        verify(dbService, times(0)).createPlan(plan);
    }

    @Test
    void testCreatePlan_ValidationError_NullDetails(SoftAssertions softly) {
        fillDefaultPlan(plan);
        plan.details = null;
        softly.assertThatThrownBy(() -> planManager.createPlan(plan))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage("Argument 'plan.details' is null.");
        verify(dbService, times(0)).getPlanIdByName(plan.name);
        verify(dbService, times(0)).createPlan(plan);
    }

    private PlanPojo fillDefaultPlan(PlanPojo plan) {
        plan.name = "Test name";
        plan.details = "Test plan details";
        plan.fee = 150;
        return plan;
    }
}
