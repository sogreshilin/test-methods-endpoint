--liquibase formatted sql

--changeset miliaev:create-tables
CREATE TABLE CUSTOMER
(
  id         TEXT    NOT NULL,
  first_name TEXT    NOT NULL,
  last_name  TEXT    NOT NULL,
  login      TEXT    NOT NULL,
  pass       TEXT    NOT NULL,
  balance    INTEGER NOT NULL DEFAULT '0'
);

CREATE TABLE PLAN
(
  id      TEXT NOT NULL,
  name    TEXT NOT NULL,
  details TEXT NOT NULL,
  fee     INTEGER NOT NULL
);

CREATE TABLE SUBSCRIPTION
(
  id          TEXT NOT NULL,
  customer_id TEXT NOT NULL,
  plan_id     TEXT NOT NULL
);
