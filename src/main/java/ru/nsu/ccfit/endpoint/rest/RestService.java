package ru.nsu.ccfit.endpoint.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang.exception.ExceptionUtils;
import ru.nsu.ccfit.endpoint.MainFactory;
import ru.nsu.ccfit.endpoint.database.data.CustomerPojo;
import ru.nsu.ccfit.endpoint.database.data.HealthCheckPojo;
import ru.nsu.ccfit.endpoint.database.data.PlanPojo;
import ru.nsu.ccfit.endpoint.database.data.RolePojo;
import ru.nsu.ccfit.endpoint.shared.Globals;
import ru.nsu.ccfit.endpoint.shared.JsonMapper;

@Path("")
public class RestService {
    @RolesAllowed({Globals.UNKNOWN_ROLE, Globals.ADMIN_ROLE})
    @GET
    @Path("/health_check")
    public Response healthCheck() {
        return Response.ok().entity(JsonMapper.toJson(new HealthCheckPojo(), true)).build();
    }

    @RolesAllowed({Globals.UNKNOWN_ROLE, Globals.ADMIN_ROLE})
    @GET
    @Path("/get_role")
    public Response getRole(@Context ContainerRequestContext crc) {
        RolePojo rolePojo = new RolePojo();
        rolePojo.role = crc.getProperty("ROLE").toString();

        return Response.ok().entity(JsonMapper.toJson(rolePojo, true)).build();
    }

    // Example request: ../customers?login='john_wick@gmail.com'
    @RolesAllowed(Globals.ADMIN_ROLE)
    @GET
    @Path("/get_customers")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomers(@DefaultValue("") @QueryParam("login") String customerLogin) {
        try {
            List<CustomerPojo> customers = MainFactory.getInstance()
                .getCustomerManager()
                .getCustomers().stream()
                .filter(x -> customerLogin.isEmpty() || x.login.equals(customerLogin))
                .collect(Collectors.toList());

            return Response.ok().entity(JsonMapper.toJson(customers, true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Globals.ADMIN_ROLE)
    @POST
    @Path("/create_customer")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createCustomer(String customerDataJson) {
        try {
            // convert json to object
            CustomerPojo customerData = JsonMapper.fromJson(customerDataJson, CustomerPojo.class);

            // create new customer
            CustomerPojo customer = MainFactory.getInstance().getCustomerManager().createCustomer(customerData);

            // send the answer
            return Response.ok().entity(JsonMapper.toJson(customer, true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Globals.ADMIN_ROLE)
    @PUT
    @Path("/customers")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateCustomer(String customerDataJson) {
        try {
            // convert json to object
            CustomerPojo customerData = JsonMapper.fromJson(customerDataJson, CustomerPojo.class);

            CustomerPojo customer = MainFactory.getInstance().getCustomerManager().updateCustomer(customerData);

            return Response.ok().entity(JsonMapper.toJson(customer, true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Globals.ADMIN_ROLE)
    @DELETE
    @Path("/customers")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteCustomer(@QueryParam("customerId") String customerId) {
        try {
            MainFactory.getInstance().getCustomerManager().removeCustomer(UUID.fromString(customerId));
            return Response.ok().entity(JsonMapper.toJson(new HealthCheckPojo(), true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Globals.ADMIN_ROLE)
    @GET
    @Path("/plans")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPlans(@QueryParam("customerId") UUID customerId) {
        try {
            List<PlanPojo> customers = new ArrayList<>(MainFactory.getInstance()
                .getPlanManager()
                .getPlans(customerId));

            return Response.ok().entity(JsonMapper.toJson(customers, true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Globals.ADMIN_ROLE)
    @POST
    @Path("/plans")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createPlan(String planDataJson) {
        try {
            // convert json to object
            PlanPojo planData = JsonMapper.fromJson(planDataJson, PlanPojo.class);

            // create new plan
            PlanPojo plan = MainFactory.getInstance().getPlanManager().createPlan(planData);

            // send the answer
            return Response.ok().entity(JsonMapper.toJson(plan, true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Globals.ADMIN_ROLE)
    @PUT
    @Path("/plans")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updatePlan(String planDataJson) {
        try {
            // convert json to object
            PlanPojo planData = JsonMapper.fromJson(planDataJson, PlanPojo.class);

            // update plan
            PlanPojo plan = MainFactory.getInstance().getPlanManager().updatePlan(planData);

            // send the answer
            return Response.ok().entity(JsonMapper.toJson(plan, true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed(Globals.ADMIN_ROLE)
    @DELETE
    @Path("/plans")
    public Response deletePlan(@QueryParam("planId") String planId) {
        try {
            MainFactory.getInstance().getPlanManager().removePlan(UUID.fromString(planId));

            // send the answer
            return Response.ok().entity(JsonMapper.toJson(new HealthCheckPojo(), false)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" + ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }
}
