package ru.nsu.ccfit.endpoint.manager;

import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import ru.nsu.ccfit.endpoint.database.IDBService;
import ru.nsu.ccfit.endpoint.database.data.PlanPojo;

public class PlanManager extends ParentManager {
    public PlanManager(IDBService dbService, Logger flowLog) {
        super(dbService, flowLog);
    }

    /**
     * Метод создает новый объект типа Plan. Ограничения:
     * name - длина не больше 128 символов и не меньше 2 включительно не содержит спец символов. Имена не пересекаются друг с другом;
     * /* details - длина не больше 1024 символов и не меньше 1 включительно;
     * /* fee - больше либо равно 0 но меньше либо равно 999999.
     */
    public PlanPojo createPlan(PlanPojo plan) {
        validatePlan(plan);
        return dbService.createPlan(plan);
    }

    public PlanPojo updatePlan(PlanPojo plan) {
        validatePlan(plan);
        try {
            dbService.getPlanById(plan.id);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Plan with id " + plan.id + " does not exists");
        }
        return dbService.updatePlanById(plan.id, plan);
    }

    public void removePlan(UUID id) {
        dbService.removePlanById(id);
    }

    /**
     * Метод возвращает список планов доступных для покупки.
     */
    public List<PlanPojo> getPlans(UUID customerId) {
        return dbService.getAvailablePlansForCustomer(customerId);
    }

    private void validatePlan(PlanPojo plan) {
        Validate.notNull(plan, "Argument 'plan' is null.");
        Validate.notNull(plan.name, "Argument 'plan.name' is null.");
        Validate.isTrue(
            2 <= plan.name.length() && plan.name.length() <= 128,
            "Argument 'plan.name' must be between 2 and 128 symbols"
        );
        Validate.notNull(plan.details, "Argument 'plan.details' is null.");
        Validate.isTrue(
            1 <= plan.details.length() && plan.details.length() <= 1024,
            "Argument 'plan.details' must be between 2 and 128 symbols"
        );
        Validate.isTrue(
            0 <= plan.fee && plan.fee <= 999999,
            "Argument 'plan.fee' must be between 0 and 999999"
        );
        UUID uuid = null;
        try {
            uuid = dbService.getPlanIdByName(plan.name);
        } catch (IllegalArgumentException ignored) {
        }
        Validate.isTrue(uuid == null,
            "Plan with name " + plan.name + " already exists");
    }
}
