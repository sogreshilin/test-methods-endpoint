package ru.nsu.ccfit.endpoint.manager;

import org.slf4j.Logger;
import ru.nsu.ccfit.endpoint.database.IDBService;

public class ParentManager {
    protected IDBService dbService;
    protected Logger log;

    public ParentManager(IDBService dbService, Logger log) {
        this.dbService = dbService;
        this.log = log;
    }
}
