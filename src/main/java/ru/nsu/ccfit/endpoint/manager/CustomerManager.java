package ru.nsu.ccfit.endpoint.manager;

import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.Validate;
import org.apache.commons.validator.routines.EmailValidator;
import org.slf4j.Logger;
import ru.nsu.ccfit.endpoint.database.IDBService;
import ru.nsu.ccfit.endpoint.database.data.CustomerPojo;

public class CustomerManager extends ParentManager {
    public CustomerManager(IDBService dbService, Logger flowLog) {
        super(dbService, flowLog);
    }

    /**
     * Метод создает новый объект типа Customer. Ограничения:
     * Аргумент 'customerData' - не null;
     * firstName - нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов;
     * lastName - нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов;
     * login - указывается в виде email, проверить email на корректность, проверить что нет customer с таким же email;
     * pass - длина от 6 до 12 символов включительно, не должен быть простым (123qwe или 1q2w3e), не должен содержать части login, firstName, lastName
     * money - должно быть равно 0.
     */
    public CustomerPojo createCustomer(CustomerPojo customer) {
        Validate.notNull(customer, "Argument 'customerData' is null.");

        Validate.notNull(customer.firstName);
        Validate.isTrue(!customer.firstName.contains(" "),
            "firstName should not contains whitespaces");
        Validate.isTrue(customer.firstName.length() >= 2 && customer.firstName.length() < 13,
            "firstName's length should be more or equal 6 symbols and less or equal 12 symbols.");
        Validate.isTrue(customer.firstName.matches("[A-Z][a-z]*"),
            "firstName's should begin with a capital letter, the rest of the characters are lowercase.");

        Validate.notNull(customer.lastName);
        Validate.isTrue(!customer.lastName.contains(" "),
            "lastName should not contains whitespaces");
        Validate.isTrue(customer.lastName.length() >= 2 && customer.lastName.length() < 13,
            "lastName's length should be more or equal 6 symbols and less or equal 12 symbols.");
        Validate.isTrue(customer.lastName.matches("[A-Z][a-z]*"),
            "lastName's should begin with a capital letter, the rest of the characters are lowercase.");

        Validate.notNull(customer.login);
        Validate.isTrue(EmailValidator.getInstance().isValid(customer.login),
            "login should be valid email");
        UUID uuid = null;
        try {
            uuid = dbService.getCustomerIdByLogin(customer.login);
        } catch (IllegalArgumentException ignored) {
        }
        Validate.isTrue(uuid == null,
            "Customer with login " + customer.login + " already exists");

        Validate.notNull(customer.pass);
        Validate.isTrue(customer.pass.length() >= 6 && customer.pass.length() < 13,
            "Password's length should be more or equal 6 symbols and less or equal 12 symbols.");
        Validate.isTrue(!customer.pass.equalsIgnoreCase("123qwe"), "Password is easy.");
        Validate.isTrue(!customer.pass.equalsIgnoreCase("1q2w3e"), "Password is easy.");

        Validate.isTrue(customer.balance == 0,
            "balance should be zero");

        return dbService.createCustomer(customer);
    }

    /**
     * Метод возвращает список объектов типа customer.
     */
    public List<CustomerPojo> getCustomers() {
        return dbService.getCustomers();
    }


    /**
     * Метод обновляет объект типа Customer.
     * Можно обновить только firstName и lastName.
     */
    public CustomerPojo updateCustomer(CustomerPojo customer) {
        throw new NotImplementedException("Please implement the method.");
    }

    public void removeCustomer(UUID id) {
        dbService.deleteCustomer(id);
    }

    /**
     * Метод добавляет к текущему баласу amount.
     * amount - должен быть строго больше нуля.
     */
    public CustomerPojo topUpBalance(UUID customerId, int amount) {
        throw new NotImplementedException("Please implement the method.");
    }
}
