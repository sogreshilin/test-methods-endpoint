package ru.nsu.ccfit.endpoint.database.data;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SubscriptionPojo {
    @JsonProperty("id")
    public UUID id;

    @JsonProperty("customerId")
    public UUID customerId;

    @JsonProperty("planId")
    public UUID planId;
}
