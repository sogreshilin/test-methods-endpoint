package ru.nsu.ccfit.endpoint.database.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RolePojo {
    @JsonProperty("role")
    public String role;
}
