package ru.nsu.ccfit.endpoint.database;

import java.util.List;
import java.util.UUID;

import ru.nsu.ccfit.endpoint.database.data.CustomerPojo;
import ru.nsu.ccfit.endpoint.database.data.PlanPojo;

public interface IDBService {
    CustomerPojo createCustomer(CustomerPojo customerData);

    List<CustomerPojo> getCustomers();

    UUID getCustomerIdByLogin(String customerLogin);

    PlanPojo createPlan(PlanPojo plan);

    PlanPojo getPlanById(UUID id);

    PlanPojo updatePlanById(UUID id, PlanPojo plan);

    void removePlanById(UUID id);

    List<PlanPojo> getAvailablePlansForCustomer(UUID customerId);

    UUID getPlanIdByName(String name);

    void deleteCustomer(UUID id);
}
