package ru.nsu.ccfit.endpoint.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import ru.nsu.ccfit.endpoint.database.data.CustomerPojo;
import ru.nsu.ccfit.endpoint.database.data.PlanPojo;
import ru.nsu.ccfit.endpoint.shared.JsonMapper;

public class DBService implements IDBService {
    // Constants
    private static final String INSERT_CUSTOMER = "INSERT INTO CUSTOMER(id, first_name, last_name, login, pass, balance) values ('%s', '%s', '%s', '%s', '%s', %s)";
    private static final String SELECT_CUSTOMER = "SELECT id FROM CUSTOMER WHERE login='%s'";
    private static final String SELECT_CUSTOMERS = "SELECT * FROM CUSTOMER";

    private static final String INSERT_SUBSCRIPTION = "INSERT INTO SUBSCRIPTION(id, customer_id, plan_id) values ('%s', '%s', '%s')";

    private static final String INSERT_PLAN = "INSERT INTO PLAN(id, name, details, fee) values ('%s', '%s', '%s', %s)";
    private static final String UPDATE_PLAN = "UPDATE plan SET name = '%s', details = '%s', fee = %s WHERE id = '%s'";
    private static final String GET_PLAN_BY_ID = "SELECT id, name, details, fee FROM plan WHERE id = '%s'";
    private static final String DELETE_PLAN_BY_ID = "DELETE FROM plan WHERE id = '%s'";

    private Logger logger;
    private static final Object generalMutex = new Object();
    private Connection connection;

    public DBService(Logger logger) {
        this.logger = logger;
        init();
    }

    public CustomerPojo createCustomer(CustomerPojo customerData) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'createCustomer' was called with data: \n%s", JsonMapper.toJson(customerData, true)));

            customerData.id = UUID.randomUUID();
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                    String.format(
                        INSERT_CUSTOMER,
                        customerData.id,
                        customerData.firstName,
                        customerData.lastName,
                        customerData.login,
                        customerData.pass,
                        customerData.balance));
                return customerData;
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public List<CustomerPojo> getCustomers() {
        synchronized (generalMutex) {
            logger.info("Method 'getCustomers' was called.");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(SELECT_CUSTOMERS);
                List<CustomerPojo> result = new ArrayList<>();
                while (rs.next()) {
                    CustomerPojo customerData = new CustomerPojo();

                    customerData.firstName = rs.getString(2);
                    customerData.lastName = rs.getString(3);
                    customerData.login = rs.getString(4);
                    customerData.pass = rs.getString(5);
                    customerData.balance = rs.getInt(6);

                    result.add(customerData);
                }
                return result;
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public UUID getCustomerIdByLogin(String customerLogin) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'getCustomerIdByLogin' was called with data '%s'.", customerLogin));

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                    String.format(
                        SELECT_CUSTOMER,
                        customerLogin));
                if (rs.next()) {
                    return UUID.fromString(rs.getString(1));
                } else {
                    throw new IllegalArgumentException("Customer with login '" + customerLogin + " was not found");
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public PlanPojo createPlan(PlanPojo plan) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'createPlan' was called with data '%s'.", plan));

            plan.id = UUID.randomUUID();
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                    String.format(
                        INSERT_PLAN,
                        plan.id,
                        plan.name,
                        plan.details,
                        plan.fee));
                return plan;
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    @Override
    public PlanPojo getPlanById(UUID id) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'getPlanById' was called with data '%s'.", id));

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(String.format(GET_PLAN_BY_ID, id));
                if (rs.next()) {
                    return extractPlan(rs);
                } else {
                    throw new IllegalArgumentException("Plan with id '" + id + " was not found");
                }
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    @Override
    public PlanPojo updatePlanById(UUID id, PlanPojo plan) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'updatePlanById' was called with data '%s', '%s'.", id, plan));

            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                    String.format(UPDATE_PLAN, plan.name, plan.details, plan.fee, plan.id)
                );
                return plan;
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    @Override
    public void removePlanById(UUID id) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'removePlanById' was called with data '%s'.", id));
            try {
                Statement statement = connection.createStatement();
                statement.execute(String.format(DELETE_PLAN_BY_ID, id));
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    @Override
    public List<PlanPojo> getAvailablePlansForCustomer(UUID customerId) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'getAvailablePlansForCustomer' was called with data '%s'.", customerId));
            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                    "SELECT plan.id, plan.name, plan.details, plan.fee " +
                        "FROM customer, plan " +
                        "WHERE customer.balance >= plan.fee AND customer.id = '" + customerId + "'"
                );

                List<PlanPojo> plans = new ArrayList<>();
                while (rs.next()) {
                    plans.add(extractPlan(rs));
                }
                return plans;
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    @Override
    public UUID getPlanIdByName(String name) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'getPlanIdByName' was called with data '%s'.", name));

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(String.format("select id from plan where name = '%s'", name));
                if (rs.next()) {
                    return UUID.fromString(rs.getString("id"));
                } else {
                    throw new IllegalArgumentException("Plan with name '" + name + " was not found");
                }
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    @Override
    public void deleteCustomer(UUID id) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'deleteCustomer' was called with data '%s'.", id));

            try {
                Statement statement = connection.createStatement();
                statement.execute(String.format("delete from customer where id = '%s'", id));
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    private PlanPojo extractPlan(ResultSet rs) throws SQLException {
        PlanPojo plan = new PlanPojo();
        plan.id = UUID.fromString(rs.getString("id"));
        plan.name = rs.getString("name");
        plan.details = rs.getString("details");
        plan.fee = rs.getInt("fee");
        return plan;
    }

    private void init() {
        logger.debug("-------- PostgreSQL JDBC Connection Testing ------------");
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException ex) {
            logger.debug("Where is your PostgreSQL JDBC Driver?", ex);
            throw new RuntimeException(ex);
        }

        logger.debug("PostgreSQL JDBC Driver Registered!");

        try {
            connection = DriverManager
                .getConnection(
                    "jdbc:postgresql://localhost:15800/test-methods-endpoint",
                    "user",
                    "password");
        } catch (SQLException ex) {
            logger.error("Connection Failed! Check output console", ex);
            throw new RuntimeException(ex);
        }

        if (connection != null) {
            logger.debug("You made it, take control your database now!");
        } else {
            logger.error("Failed to make connection!");
        }
    }
}
